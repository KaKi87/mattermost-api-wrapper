const
    axios = require('axios'),
    parseTimestamps = data => ({
        createdTimestamp: data['create_at'],
        updatedTimestamp: data['update_at'],
        deletedTimestamp: data['delete_at']
    }),
    parseUser = data => {
        const
            {
                'timezone': {
                    'useAutomaticTimezone': isTimezoneAutomaticJson,
                    'manualTimezone': manualTimezone,
                    'automaticTimezone': automaticTimezone
                }
            } = data,
            {
                'props': {
                    'customStatus': customStatusJson
                } = {}
            } = data,
            {
                'emoji': customStatusEmojiCode,
                'text': customStatusText
            } = customStatusJson ? JSON.parse(customStatusJson) : {},
            isTimezoneAutomatic = JSON.parse(isTimezoneAutomaticJson);
        return {
            userId: data['id'],
            ...parseTimestamps(data),
            username: data['username'],
            email: data['email'],
            nickname: data['nickname'] || undefined,
            firstName: data['first_name'] || undefined,
            lastName: data['last_name'] || undefined,
            roleList: data['roles'].split(' '),
            customStatusEmojiCode,
            customStatusText,
            pictureUpdatedTimestamp: data['last_picture_update'],
            locale: data['locale'],
            isMfaEnabled: data['mfa_active'],
            isTimezoneAutomatic,
            timezone: (isTimezoneAutomatic ? automaticTimezone : manualTimezone) || undefined,
            isBot: data['is_bot'] || false,
            botDescription: data['bot_description']
        };
    };

module.exports = ({
    domain,
    axiosAdapter
}) => {
    const client = axios.create({
        baseURL: `https://${domain}/api/v4`,
        adapter: axiosAdapter
    });
    return {
        login: async ({
            login,
            password,
            mfaToken
        }) => {
            const {
                headers: {
                    'token': sessionToken
                },
                data
            } = await client.post('users/login', {
                'login_id': login,
                password,
                'token': mfaToken
            });
            return {
                sessionToken,
                user: parseUser(data),
                account: {
                    isEmailVerified: data['email_verified'],
                    areEmailNotificationsEnabled: JSON.parse(data['notify_props']['email']),
                    pushNotificationMode: data['notify_props']['push'],
                    desktopNotificationMode: data['notify_props']['desktop'],
                    isDesktopNotificationSoundEnabled: JSON.parse(data['notify_props']['desktop_sound']),
                    mentionKeywordList: data['notify_props']['mention_keys'].split(' ').filter(Boolean),
                    areChannelNotificationsEnabled: JSON.parse(data['notify_props']['channel']),
                    areFirstNameNotificationsEnabled: JSON.parse(data['notify_props']['first_name']),
                    passwordUpdatedTimestamp: data['last_password_update'],
                    failedLoginAttemptCount: data['failed_attempts'] || 0
                }
            };
        },
        session: ({
            token
        }) => {
            const headers = { 'Authorization': `Bearer ${token}` };
            return {
                getUsers: async () => (await client.get('users', { headers })).data.map(parseUser),
                getTeams: async () => (await client.get('users/me/teams', { headers })).data.map(data => ({
                    teamId: data['id'],
                    ...parseTimestamps(data),
                    displayName: data['display_name'],
                    name: data['name'],
                    description: data['description']
                })),
                team: ({
                    teamId
                }) => ({
                    getChannels: async () => {
                        const
                            [
                                { data: data1 },
                                { data: data2 }
                            ] = await Promise.all([
                                client.get(`users/me/teams/${teamId}/channels`, { headers }),
                                client.get(`users/me/teams/${teamId}/channels/categories`, { headers })
                            ]),
                            channels = data1.map(data => ({
                                channelId: data['id'],
                                ...parseTimestamps(data),
                                teamId: data['team_id'] || undefined,
                                displayName: data['display_name'] || undefined,
                                name: data['name'],
                                header: data['header'] || undefined,
                                description: data['purpose'] || undefined,
                                lastMessageTimestamp: data['last_post_at'],
                                messageCount: data['total_msg_count'],
                                createdBy: data['creator_id'] || undefined
                            })).reduce((result, channel) => ({
                                ...result,
                                [channel.channelId]: channel
                            }), {}),
                            {
                                channelList: [{ channelList }],
                                categoryList = [],
                                dmChannelList: [{ channelList: dmChannelList }],
                                favoriteChannelList: [{ channelList: favoriteChannelList }]
                            } = data2['categories'].map(data => ({
                                categoryId: data['id'],
                                displayName: data['display_name'],
                                type: data['type'],
                                channelList: data['channel_ids'].map(channelId => ({ channelId }))
                            })).reduce((result, category) => {
                                const property = {
                                    'channels': 'channelList',
                                    'custom': 'categoryList',
                                    'direct_messages': 'dmChannelList',
                                    'favorites': 'favoriteChannelList'
                                }[category.type];
                                return {
                                    ...result,
                                    [property]: [...(result[property] || []), category]
                                };
                            }, {});
                        return {
                            channelList: channelList.map(({ channelId }) => channels[channelId]),
                            categoryList: categoryList.map(category => ({
                                ...category,
                                channelList: category.channelList.map(({ channelId }) => channels[channelId])
                            })),
                            dmChannelList: dmChannelList.map(({ channelId }) => channels[channelId]),
                            favoriteChannelList: favoriteChannelList.map(({ channelId }) => channels[channelId])
                        };
                    }
                }),
                channel: ({
                    channelId
                }) => ({
                    getMessages: async () => {
                        const {
                            data: {
                                'order': messageIdList,
                                'posts': messages
                            }
                        } = await client.get(`channels/${channelId}/posts`, { headers });
                        return messageIdList.map(messageId => ({
                            messageId,
                            ...parseTimestamps(messages[messageId]),
                            editedTimestamp: messages[messageId]['edit_at'],
                            userId: messages[messageId]['user_id'],
                            channelId,
                            content: messages[messageId]['message'],
                            hashtags: messages[messageId]['hashtags'].split(' ').filter(Boolean),
                            attachmentList: (messages[messageId]['metadata']['files'] || []).map(data => ({
                                attachmentId: data['id'],
                                ...parseTimestamps(data),
                                fileName: data['name'],
                                fileExtension: data['extension'],
                                fileSize: data['size'],
                                fileMimeType: data['mime_type']
                            })),
                        })).reverse();
                    }
                })
            };
        }
    };
};